## **MixMyVisit JSON video renderer**

This solution analyzes JSON files with a pre-determined structure in order to render them to a .mp4 file. The same JSON file is also used in the MixMyVisit web app  to render videos using BBC videocontext webgl player. 

This renderer creates an instance of a socket connection with the MixMyVisit web API to receive videos to render in realtime and an instance of an headlessBrowser to run the webGL VideoContext in order to get the buffer data from the lottie-web animation frames which are later piped to a process where video and audio are merged using ffmpeg and transitions and effects are included using OpenGL, mimicking the same shaders used in the VideoContext webGL solution.



:warning: **This guide is tailored for Ubuntu Linux distributions.**



### :floppy_disk:  How to install:

 	1. Open your terminal :shell:
 	2. Run the following commands to install necessary dependencies:

​	```$ sudo npm install electron --unsafe-perm=true```

​	```$ sudo apt-get install -y libgtk2.0-0 libnotify-bin libgconf-2-4 libnss3 libxss1 ffmpeg```	

> In order to properly run **FFmpeg** version need to be **equal** or **higher** than **3.0**. To update **FFmpeg** run

​	```$ sudo add-apt-repository ppa:jonathonf/ffmpeg-4```

​	```$ sudo apt update && sudo apt-get install ffmpeg```

> IN CASE THE RENDERING PROCESS IS GOING TO BE CPU BASED INSTEAD OF GPU XVFB NEED TO BE INSTALLED

​	```$ sudo apt-get install -y xvfb```

 3. Clone this repository

 4. Change current directory and run the script

    ```$ cd ireporter_renderer```

    ```$ npm start```

> IN CASE THE RENDERING PROCESS IS GOING TO BE CPU BASED INSTEAD OF GPU RUN THE FOLLOWING COMMANDS BEFORE RUNNING THE SCRIPT

​	```$ export DISPLAY=':99.0'```
​	```	$ Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &```



:warning:  **::::** **If you wish to automate this worker we recommend using <u>systemd</u> or <u>supervisor</u>. Have in mind if the machine used to render supports GPU based rendering or not !!! ::::  ** :warning:

An example of a **systemd** **service** could be the following:

​	**Environment=DISPLAY=:99**
​	**User=root**
​	**WorkingDirectory= $PATH_TO_RENDERER_SCRIPT**
​	**ExecStart=/usr/bin/npm start**


​	**Restart=on-failure**

​	**[Install]**
​	**WantedBy=multi-user.target**



### :panda_face:  How does it work:

The process of creating the video is divided into **5** steps, in which for one of them it is required to use an **headless-browser** <u>(src/headless_browser)</u> such as **Electron**. 

Before starting to render a video, a socket connection is open  between the renderer and the MixMyVisit API in order to listen for calls to process a video (src/headless_browser/socket) . This connection must always remain open while the renderer is running. The socket connection is also run in the **headless-browser**.

A local express server <u>(src/local_requests)</u>, running in the port **6969**, is used to communicate the **headless-browser** process with the remaining processes which require system functions such as **fs** or **FFmpeg**.

To manage the video creation processes, a process manager called **Maestro** <u>(/src/maestro)</u> renders the videos, deals with asynchronous tasks and cleans up the finished processes. 

The **Render **class <u>(/src/render)</u> deals with the actual FFmpeg commands to render the videos.

When a JSON video file is received via the open socket connection, both the rendering of the titles and the initial processing of the video clips (cutting and concating with openGL transitions and effects) are done asynchronously. After each process is over, a request is sent to the **local-server** in order to communicate **Maestro** the current state of the rendering process and to start the **Final** **render** **process**, where audio and overlays are added to video as well as some other necessary content.

Through the entire process the renders are temporarily saved in a temp folder created as soon as a video is received through the socket connection. The name of this temporary folder, created in /tmp is unique, created dynamically and saved through the entire process until **Maestro** cleans it.

​	**. Titles canvas to stream** 

​		src/headless_browser/extract_frames -> src/headless_browser/extract_frames -> src/local_requests

The first step of the process is the only one which requires the headless browser. During this step the title animations are transformed from a canvas animation, using lottie-web, to a stream of images         

 uses Lottie-web / Canvas / axios

**​	. Title rendering ** 

​		src/headless_browser/maestro -> src/headless_browser/renderer -> src/local_requests

​		The stream of images is transformed to an .apng to be used as an overlay over the final video in the **final render**

​	**. Video cut**

​		src/headless_browser/maestro -> src/headless_browser/renderer

​		The used clips are cut according to the mark-ins and mark-outs

​	**. Video concat**

​		src/headless_browser/renderer -> src/headless_browser/renderer  -> src/local_requests

​		The cut videos are merged together using openGL to add transitions and effects using shaders both present in openGL and BBC's Videocontext webGL player

​	**. Final render**

​		src/headless_browser/maestro -> src/headless_browser/renderer

​		 Audio and overlays are added to the concated video as well as some other required content.



### :smiley:  JSON structure template:

```{
	id: video_id,
	metadata: {
		type: "mp4",
		preset: "ultrafast",
		profile: "baseline",
		resolution: {
			width: 1920,
			height: 1080
		},
		fps: 24,
		encoding: {
			video: "libx264",
			audio: "libmp3lame"
		}
	},
	video: {
		title: "Title_for_the_video",
		template: "TEMPLATE_01",
		platforms: {
			facebook: {
				clips: [
					{
						url:
							"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4",
						trimStart: 0,
						duration: 15,
						volume: 0.5
					},
					{
						url:
							"http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4",
						trimStart: 0,
						duration: 15,
						volume: 0.2
					}
				],
				overlays: {
					titles: [
						{
							id: "FACEBOOK.slide_left",
							title: "Cristiano Ronaldo",
							subtitle: "Ataca de Novo 1",
							markIn: 2,
							markOut: 8
						},
						{
							id: "FACEBOOK.slide_left",
							title: "Cristiano",
							subtitle: "Ataca de Novo 2",
							markIn: 4,
							markOut: 8
						},
						{
							id: "FACEBOOK.slide_left",
							title: "Cristiano Ronaldo",
							subtitle: "Ataca de Novo 3",
							markIn: 5,
							markOut: 7
						}
					],
					images: [
						{
							type: "static",
							url:
								"https://vignette.wikia.nocookie.net/fantendo/images/6/6e/Small-mario.png/revision/latest/scale-to-width-down/381?cb=20120718024112",
							markIn: 0,
							markOut: 4,
							position: {
								x: 20,
								y: 50
							}
						},
						{
							type: "gif",
							url:
								"https://media.giphy.com/media/13gvXfEVlxQjDO/giphy.gif",
							markIn: 3,
							markOut: 10,
							position: {
								x: 15,
								y: 20
							}
						},
						{
							type: "gif",
							url: "https://i.imgur.com/KOXOBiN.gif",
							markIn: 2,
							markOut: 10,
							position: {
								x: 40,
								y: 80
							}
						}
					]
				},
				audios: [
					{
						url:
							"http://www.largesound.com/ashborytour/sound/brobob.mp3",
						trimStart: 0,
						markIn: 10,
						markOut: 20,
						volume: 0.3
					},
					{
						url:
							"http://www.hochmuth.com/mp3/Haydn_Cello_Concerto_D-1.mp3",
						trimStart: 60,
						markIn: 1,
						markOut: 20,
						volume: 0.3
					}
				]
			}
		},
		processing: {
			transitions: ["CROSSFADE", "CROSSFADE", "CROSSFADE"],
			effects: []
		}
	}
}
```