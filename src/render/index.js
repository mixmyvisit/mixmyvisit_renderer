import axios from "axios";
import FormData from "form-data";
import fs from "fs";
import ffmpeg from "fluent-ffmpeg";
import probe from "node-ffprobe";
import concat from "ffmpeg-concat";
import _ from "lodash";
import uuid from "uuid/v1";
import notify from "../notify";
import transitions from "../templates/transitions";

export default class {
  constructor(tmpPath, id, video, tagId, username, metadata) {
    this.video = video;
    this.metadata = metadata;
    this.tmpPath = tmpPath;
    this.video_id = id;
    this.username = username;
    this.tagId = tagId;
  }

  fadeDuration = 0.3;

  processClips() {
    console.log("processing clips");
    const video_id = this.video_id;
    const tmpPath = this.tmpPath;
    console.log("***************************TmpPATH =>", tmpPath);
    return this.cutClips((clips, platform) => {
      this.concatClips(clips, platform)
        .then(() => {
          let formData = {
            path: tmpPath,
            platform: platform,
            element: "video",
          };

          axios
            .post(
              `${process.env.LOCAL_SERVER}:${process.env.LOCAL_PORT}/maestro/update`,
              formData
            )
            .then(function (response) {})
            .catch(function (error) {
              console.log("render index maestro update error");
              console.log(error);
              notify(
                video_id,
                "error",
                "ERROR ON Stage: Processing Clips",
                "/maestro/update"
              );
            });
        })
        .catch(function (error) {
          console.log("ERROR ON Stage: Concatening Clips");
          console.log(error);
          notify(video_id, "error", "ERROR ON Stage: Concatening Clips");
        });
    }).catch(function (error) {
      console.log("ERROR ON Stage: Cutting Clips");
      notify(video_id, "error", "ERROR ON Stage: Cutting Clips");
    });
  }

  cutClips(callback) {
    const video = this.video;
    const video_id = this.video_id;
    const metadata = this.metadata;
    const tmpPath = this.tmpPath;
    const username = this.username;

    let renderedClips = [];
    return new Promise((resolve, reject) => {
      _.each(video.platforms, (platformObject, platform) => {
        renderedClips[platform] = new Array(platformObject.clips.length);
      });

      _.each(video.platforms, (platformObject, platform) => {
        console.log("##### CUTTING CLIPS #####");
        _.each(platformObject.clips, (clip, key, clips) => {
          let type;
          if (clip.url.split(".").pop() !== "mp4") type = "jpg";
          else type = metadata.type;
          let outputPath = `${tmpPath}/clip_${platform}_${key}.${type}`;
          let startTime = clip.trimStart || 0;
          //If duration wasn't set or is null is converted to 0
          let duration = clip.duration || 0;

          let clipComment;
          let isSimpleVisit = false;

          if (clip.location === "start") {
            const splitComment = clip.comment.split("?");
            isSimpleVisit = splitComment[0] === "" || splitComment[0] === " "
            clipComment = splitComment;
          } else if (clip.comment) {
            clipComment = clip.comment;
          } else if (clip.location) {
            clipComment = clip.location;
          } else {
            clipComment = "";
          }

          probe(clip.url, function (err, probeData) {
            let videoDuration;
            if (probeData.format.duration)
              videoDuration = probeData.format.duration;
            else videoDuration = 10;
            //If duration wasn't set or is null or is 0 or is negative number, duration  = the total execution time for the given file
            if (duration <= 0) {
              duration = videoDuration;
            }

            //First set StartTime if it surpass the file time it is forced to be set to 0
            if (startTime > videoDuration) {
              startTime = 0;
            }

            //Second set duration so it doesnt surpass the file time
            if (duration - startTime > videoDuration - startTime) {
              duration = videoDuration - startTime;
            }

            const fontFamily = process.env.COMMENT_FONT_FAMILY;
            const introTexts = isSimpleVisit ?
              [
                {
                  filter: "drawtext",
                  options: {
                    fontfile: fontFamily,
                    text: 'Universidade de Aveiro',
                    fontsize: 46,
                    fontcolor: "white",
                    x: "(w-text_w)/2",
                    y: "(h-text_h)/2",
                    box: 1,
                    boxcolor: "black@0.3",
                    boxborderw: 16,
                    fix_bounds: true,
                  },
                },
                {
                  filter: "drawtext",
                  options: {
                    fontfile: fontFamily,
                    text: clipComment[1],
                    fontsize: 36,
                    fontcolor: "white",
                    x: "(w-text_w)/2",
                    y: "(h-text_h)-50",
                    box: 1,
                    boxcolor: "black@0.3",
                    boxborderw: 16,
                    fix_bounds: true,
                    expansion: "none",
                  },
                },
              ]
              :
              [
                {
                  filter: "drawtext",
                  options: {
                    fontfile: fontFamily,
                    text: clipComment[0],
                    fontsize: 46,
                    fontcolor: "white",
                    x: "(w-text_w)/2",
                    y: "(h-text_h)/2",
                    box: 1,
                    boxcolor: "black@0.3",
                    boxborderw: 16,
                    fix_bounds: true,
                  },
                },
                {
                  filter: "drawtext",
                  options: {
                    fontfile: fontFamily,
                    text: clipComment[1],
                    fontsize: 36,
                    fontcolor: "white",
                    x: "(w-text_w)/2",
                    y: "(h-text_h)-50",
                    box: 1,
                    boxcolor: "black@0.3",
                    boxborderw: 16,
                    fix_bounds: true,
                    expansion: "none",
                  },
                },
                {
                  filter: "drawtext",
                  options: {
                    fontfile: fontFamily,
                    text: 'Universidade de Aveiro',
                    fontsize: 36,
                    fontcolor: "white",
                    x: "(w-text_w)/2",
                    y: "420",
                    box: 1,
                    boxcolor: "black@0.3",
                    boxborderw: 16,
                    fix_bounds: true,
                  },
                }
              ];

            const filters =
              clip.location === "start"
                ? [
                    `scale=${metadata.resolution.width}:${metadata.resolution.height}:force_original_aspect_ratio=decrease,pad=${metadata.resolution.width}:${metadata.resolution.height}:-1:-1:color=black`, ...introTexts
                  ]
                : [
                    `scale=${metadata.resolution.width}:${metadata.resolution.height}:force_original_aspect_ratio=decrease,pad=${metadata.resolution.width}:${metadata.resolution.height}:-1:-1:color=black`,
                    {
                      filter: "drawtext",
                      options: {
                        fontfile: fontFamily,
                        text: clipComment,
                        fontsize: 36,
                        fontcolor: "white",
                        x: "(w-text_w)/2",
                        y: "(h-text_h)-50",
                        box: 1,
                        boxcolor: "black@0.3",
                        boxborderw: 16,
                        fix_bounds: true,
                        expansion: "none",
                      },
                    },
                  ];

            if (type === "mp4") {
              duration = ffmpeg()
                .input(clip.url)
                //.input('/data/renderer/mixmyvisit_renderer/src/assets/mmv-logo-clean-with-letters.png')
                .setStartTime(startTime)
                .setDuration(duration)
                .on("error", function (err) {
                  notify(
                    video_id,
                    "error",
                    "ERROR ON Stage: Cutting Clips",
                    "cutting clips",
                    err
                  );
                  console.log("An error occurred: " + err.message);
                  reject();
                })
                .on("end", function () {
                  notify(
                    video_id,
                    "progress",
                    "FINISHED Stage: Cutting Clips",
                    "100"
                  );
                  console.log("Processing finished !");
                  clips[key].inputPath = outputPath;
                  renderedClips[platform][key] = outputPath;
                  if (
                    renderedClips[platform].filter((x) => _.isString(x))
                      .length === clips.length
                  ) {
                    callback(clips, platform);
                  }
                })
                .on("progress", function (progress) {
                  console.log("Processing: " + progress.percent);
                })
                .on("start", function (command) {
                  notify(
                    video_id,
                    "progress",
                    "STARTED Stage: Cutting Clips",
                    "0"
                  );
                  console.log("Start => ", command);
                })
                .format(metadata.type)
                .addOptions([`-preset ${metadata.preset}`])
                .addOptions([`-profile ${metadata.profile}`])
                .videoCodec(metadata.encoding.video)
                .videoFilters(filters)
                .noAudio()
                //.size(`${metadata.resolution.width}x${metadata.resolution.height}`)
                //.autopad()
                .fps(metadata.fps)
                //.complexFilter([
                //  "[0:v]scale=640:-1[bg];[bg][1:v]overlay=W-w-10:H-h-10"
                //])
                .save(outputPath);
            } else {
              duration = ffmpeg()
                .input(clip.url)
                //.input('/data/renderer/mixmyvisit_renderer/src/assets/mmv-logo-clean-with-letters.png')
                .setDuration(clip.duration)
                .on("error", function (err) {
                  notify(
                    video_id,
                    "error",
                    "ERROR ON Stage: Cutting Clips",
                    "cutting clips",
                    err
                  );
                  console.log("An error occurred: " + err.message);
                  reject();
                })
                .on("end", function () {
                  notify(
                    video_id,
                    "progress",
                    "FINISHED Stage: Cutting Clips",
                    "100"
                  );
                  console.log("Processing finished !");
                  clips[key].inputPath = outputPath;
                  renderedClips[platform][key] = outputPath;
                  if (
                    renderedClips[platform].filter((x) => _.isString(x))
                      .length === clips.length
                  ) {
                    callback(clips, platform);
                  }
                })
                .on("progress", function (progress) {
                  console.log("Processing: " + progress.percent);
                })
                .on("start", function (command) {
                  notify(
                    video_id,
                    "progress",
                    "STARTED Stage: Cutting Clips",
                    "0"
                  );
                  console.log("Start => ", command);
                })
                .format("image2")
                .videoFilters(filters)
                //.complexFilter([
                //  "[0:v]scale=640:-1[bg];[bg][1:v]overlay=W-w-10:H-h-10"
                //])
                //.size(`${metadata.resolution.width}x${metadata.resolution.height}`)
                //.autopad()
                .save(outputPath);
            }
          });
        });
      });
    });
  }

  concatClips(clips, platform) {
    const metadata = this.metadata;
    const video_id = this.video_id;
    const tmpPath = this.tmpPath;
    const fadeDuration = this.fadeDuration;
    const outputPath = `${tmpPath}/${platform}_video.${metadata.type}`;

    return new Promise((resolve, reject) => {
      let ffmpegCmd = ffmpeg();
      let complexFilter = [];
      let inputFilter = [];
      let fadeOutStart;
      let qttClips = clips.length;
      let accDuration = 0;

      _.each(clips, (clip, index, clips) => {
        console.log("#### CONCAT CLIPS ####");

        if (clip.inputPath.split(".").pop() !== "mp4") {
          ffmpegCmd.input(clip.inputPath);
          ffmpegCmd.inputOptions(["-loop 1"]);
          ffmpegCmd.inputOptions(["-framerate 24"]);
          ffmpegCmd.inputOptions([`-t ${clip.duration}`]);
        } else ffmpegCmd.input(clip.inputPath);
        fadeOutStart = clip.duration - fadeDuration;
        inputFilter.push(`[${index}:v]format=pix_fmts=yuva420p`);
        if (index > 0) {
          // if not first video/image
          inputFilter.push(`fade=t=in:st=0:d=${fadeDuration}:alpha=1`);
        }
        if (index < qttClips - 1) {
          // if not last video/image
          inputFilter.push(
            `fade=t=out:st=${fadeOutStart}:d=${fadeDuration}:alpha=1`
          );
        }
        inputFilter.push(`setpts=PTS-STARTPTS+${accDuration}/TB[v${index}]`);
        accDuration += fadeOutStart;
        complexFilter.push(inputFilter.join());
        // clears array
        inputFilter.length = 0;
      });
      complexFilter.push(
        `[${clips.length}:v]trim=duration=${accDuration}[over0]`
      );

      for (let index = 0; index < clips.length; index++) {
        complexFilter.push(
          `[over${index}][v${index}]overlay[over${index + 1}]`
        );
      }
      ffmpegCmd
        .input(
          `color=black:s=${metadata.resolution.width}x${metadata.resolution.height}`
        )
        .inputFormat("lavfi")
        .addOptions(`-map [over${clips.length}]`)
        .videoCodec(metadata.encoding.video)
        .complexFilter(complexFilter)
        .on("end", function () {
          notify(
            video_id,
            "progress",
            "FINISHED Stage: Concatening Clips",
            "100"
          );
          console.log("Processing finished");
          resolve();
        })
        .on("progress", function (progress) {
          console.log("Processing: " + progress.percent);
        })
        .on("start", function (command) {
          notify(video_id, "progress", "STARTED Stage: Concatening Clips", "0");
          console.log("Start => ", command);
        })
        .on("error", function (err, stdout, stderr) {
          notify(video_id, "error", "ERROR ON Stage: Concatening Clips");
          console.log("An error occurred ON CONCAT CLIPS: " + err.message);
          console.log("error => ", error);
          console.log("stdout => ", stdout);
          console.log("stderr => ", stderr);
          reject();
        })
        .save(outputPath);
    });
  }

  renderFinal(platform) {
    return new Promise((resolve, reject) => {
      const video = this.video;
      const metadata = this.metadata;
      const tmpPath = this.tmpPath;
      const video_id = this.video_id;
      const complexFilter = [];

      //video inputs
      let inputs = 0;
      //video stream for inputs
      let v_output = 0;
      let a_output = 0;
      //duration to use when setting he audioStreams for the clips
      let duration = 0;

      let ffmpegCmd = ffmpeg();

      //input main video
      ffmpegCmd.input(`${tmpPath}/${platform}_video.${metadata.type}`);

      //configure audio from videos
      console.log(
        "##### CONFIGURING ORIGINAL AUDIO TO BE PUT BACK IN (MERGE) #####"
      );

      const fadeDuration = this.fadeDuration;

      _.each(video.platforms[platform].clips, (clip, index) => {
        console.log("is item video? =>", clip.url.split(".").pop() === "mp4");
          //input video with no-audio option
          const audioInput = clip.url.split(".").pop() === "mp4" ? clip.url : "/data/renderer/mixmyvisit_renderer/src/assets/ryyzn-something-bout-july.mp3";
          ffmpegCmd.input(audioInput);
          ffmpegCmd.inputOptions([`-ss ${clip.trimStart || 0}`]);
          ffmpegCmd.inputOptions([`-t ${clip.duration}`]);

          inputs += 1;
          a_output += 1;

          complexFilter.push(
              `[${inputs}:a]adelay=${duration * 1000}|${duration *
              1000},aformat=sample_fmts=fltp:sample_rates=44100:channel_layouts=stereo,volume=1[a${a_output}]`
          );

        //From the second clip onwards, half a second is anticipated from the beginning of each audio clip
        //to keep audio in sync and compensating for the effect of transitions (ffmpeg-concat package)
        duration += clip.duration - fadeDuration;
      });

      //complex filter for mixing previous adelays into one big amix
      let amixFilterString = "";
      for (let output_label = 1; output_label <= a_output; output_label++) {
        amixFilterString += `[a${output_label}]`;
      }
      a_output++;
      complexFilter.push(
        `${amixFilterString} amix=inputs=${a_output - 1} [a${a_output}]`
      );

      console.log(metadata.profile, metadata.preset);

      // setting watermark logo image
      //const lastInput = inputs++;

      //ffmpegCmd.input(
      //  `/data/renderer/mixmyvisit_renderer/src/assets/mmv-logo-clean-with-letters.png`
      //);
      //complexFilter.push(`[${lastInput}]format=rgba,colorchannelmixer=aa=0.5,scale=50:50`);
      //complexFilter.push(`[0:v][${lastInput}]overlay=W-w-5:H-h-5`);

      //complex filter for resolution
      // if condition for when there is no image
      console.log(`v_output is ${v_output}`);
      let output_res;
      if (v_output === 0) {
        output_res = "0:v";
      } else {
        output_res = `v${v_output}`;
      }
      complexFilter.push(
        `[${output_res}]scale=${metadata.resolution.width}:${metadata.resolution.height} [final]`
      );

      //map the correct outputs/ both  video and audio
      ffmpegCmd.addOptions(["-map [final]", `-map [a${a_output}]`]);

      //configure complexFilters
      ffmpegCmd.complexFilter(complexFilter);

      let _uuid = uuid();
      //encode video and audio format
      console.log("##### MERGING AND MIXING AUDIO INTO FINAL VIDEO #####");
      ffmpegCmd
        .format(metadata.type)
        .videoCodec(metadata.encoding.video)
        .audioCodec(metadata.encoding.audio)
        .autopad()
        .fps(metadata.fps)
        .addOptions(`-profile ${metadata.profile}`)
        .addOptions(`-preset ${metadata.preset}`)
        .on("error", function (err) {
          notify(video_id, "error", "ERROR ON Stage: Final Rendering");
          console.log(
            "An error occurred on MERGING AND MIXING AUDIO INTO FINAL VIDEO: " +
              err.message
          );
        })
        .on("end", function (stdout, stderr) {
          notify(
            video_id,
            "progress",
            "FINISHED Stage: Final Rendering",
            "100"
          );
          console.log("Processing finished !");

          axios
            .put(
              `${process.env.API_SERVER}/mixmvs/${video_id}?notify=rendered`,
              // changed line below --> clips/... to env variable
              { path: `clips/${_uuid}.${metadata.type}` },
              {
                headers: {
                  Authorization: `Bearer ${process.env.API_CLIENT}`,
                },
              }
            )
            .then(function (response) {
              console.log(
                "VIDEO ",
                `${process.env.CDN_PATH}/${_uuid}.${metadata.type}`,
                " INSERTED IN DATABASE => ",
                response.status
              );
            })
            .catch(function (error) {
              notify(
                video_id,
                "error",
                "ERROR ON Stage: Notify Backend",
                "notify=rendered " + "error"
              );
              console.log(error);
            });

          resolve();
        })
        .on("progress", function (progress) {
          console.log("Processing: " + progress.percent);
        })
        .on("start", function (command) {
          notify(video_id, "progress", "STARTED Stage: Final Rendering", "0");
          console.log("Start => ", command);
        })
        .on("error", function (error, stdout, stderr) {
          notify(video_id, "error", "ERROR ON Stage: Final Rendering");
          console.log("error => ", error);
          console.log("stdout => ", stdout);
          console.log("stderr => ", stderr);
          reject();
        })
        .save(`${process.env.CDN_PATH}/${_uuid}.${metadata.type}`);
    });
  }
}
