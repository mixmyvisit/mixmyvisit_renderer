import express from "express";
import multer from "multer";
import bodyParser from "body-parser";
import cors from "cors";
import fs from "fs";
import ffmpeg from "fluent-ffmpeg";
import _ from "lodash";
import tmp from "tmp";
import { Readable } from "stream";
import Maestro from "../maestro";
import notify from "../notify";

export default new Promise(function (resolve, reject) {
  const upload = multer();
  const parser = bodyParser.json();
  const app = express();
  app.use(cors());

  const maestro = new Maestro();

  app.get("/folder/temp", function (req, res, next) {
    try {
      var tmpobj = tmp.dirSync({
        mode: 0o750,
        template: "/tmp/tmp-mixmyvisit_XXXXXX",
      });
      res.json({ path: tmpobj.name, success: true });
    } catch (err){
      console.log(err)
    }
  });

  app.post("/headless/notify", parser, function (req, res, next) {
    let key = req.body.key || undefined;
    let id = req.body.id || undefined;
    let status = req.body.status || undefined;
    let reason = req.body.reason || undefined;
    console.log(status, reason);
    notify(id, key, status, reason);
    res.json({ request: "/headless/notify", success: true });
  });

  app.post("/render/clips", parser, function (req, res, next) {
    let path = req.body.path;
    let method = "renderClips";

    console.log(path, method);

    maestro.callMethod(path, method);
    res.json({ request: "/render/clips", success: true });
    //			.then(function() {
    //				res.json({ request: "/render/clips", success: true });
    //		});
    //		res.json({ request: "/render/clips", success: true });
  });

  app.post("/maestro/update", parser, function (req, res, next) {
    let path = req.body.path;
    let platform = req.body.platform;
    let element = req.body.element;
    maestro.updateProcess(path, platform, element);

    res.json({ request: "/maestro/update", success: true });
  });

  app.post("/maestro/add", parser, function (req, res, next) {
    let path = req.body.path;
    let id = req.body.id;
    let video = req.body.video;
    let metadata = req.body.metadata;
    let tagId = req.body.tagId;
    let username = req.body.username;

    maestro.addProcess(path, id, video, tagId, username, metadata);

    res.json({ request: "/maestro/add", success: true });
  });

  app.listen(process.env.LOCAL_PORT);
  resolve();
}).catch({});
