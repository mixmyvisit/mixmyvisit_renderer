import axios from "axios";

function headlessNotify(video_id, key, status, reason) {
    console.log("headless notify =>,", video_id, key, status, reason)
  let formData = {
    id: video_id,
    key: key,
    status: status,
    reason: reason,
  };
  axios
    .post("http://127.0.0.1:6969/headless/notify", formData)
    //		.then(function (response) {
    //		})
    .catch(function (error) {
      console.log(error);
    });
}

function notify(video_id, key, status, reason) {
    console.log("notify =>,", video_id, key, status, reason)
  axios
    .put(
      `${process.env.API_SERVER}/mixmvs/${video_id}?notify=${key}`,
      {
        ...(reason && {
          reason: `${reason}`,
        }),
        ...(status && {
          status: `${status}`,
        }),
      },
      {
        headers: {
          Authorization: `Bearer ${process.env.API_CLIENT}`,
        },
      }
    )
    .catch(function (error) {
      console.log(JSON.stringify(error.response.data.message));
    });
}

export { headlessNotify, notify };
export default notify;
