import fs from "fs";
import ffmpeg from "fluent-ffmpeg";
import _ from "lodash";
import probe from "node-ffprobe";
import transitions from "../templates/transitions";
import Render from "../render";
import rimraf from "rimraf";
import notify from "../notify";

export default class {
  constructor() {
    //we use the temporary folder name as identifier because it is supposed to be unique
    this.processes = {};
  }

  addProcess(tmpPath, id, video, tagId, username, metadata) {
    let platforms = {};

    try {
      _.each(video.platforms, (platformObject, platform) => {
        console.log(platformObject.overlays);
        platforms[platform] = {
          video: {
            id: id,
            length: 1,
            rendered: 0,
          },
          rendered: false,
        };
      });
      this.processes[tmpPath] = {
        process: new Render(tmpPath, id, video, tagId, username, metadata),
        platforms: platforms,
      };
    } catch (e) {
      this.cleanProcess(tmpPath);
      console.log("error: ", e);
    }
  }

  callMethod(tmpPath, method, data) {
    if (method === "renderClips") {
      console.log('did call method?')
      try {
        return this.processes[tmpPath].process.processClips();
      } catch (err){
        this.cleanProcess(tmpPath);
        console.log(err)
      }
    }
  }

  updateProcess(tmpPath, platform, element) {
    let platformProcess = this.processes[tmpPath].platforms[platform];
    platformProcess[element].rendered += 1;
    console.log('update process', tmpPath, platform, element);

    console.log('cleanup condition', platformProcess.video.rendered >= platformProcess.video.length)

    if (
      platformProcess.video.rendered >= platformProcess.video.length //&&
      // platformProcess.titles.rendered >=
      // 	platformProcess.titles.length
    ) {
      //call methor renderFinal for platform for a certain video process
      this.processes[tmpPath].process
        .renderFinal(platform)
        .then(() => {
          this.processes[tmpPath].platforms[platform].rendered = true;

          console.log('is finalizing')
          this.cleanProcess(tmpPath);
        })
        .catch((err) => {
          this.cleanProcess(tmpPath);
          console.log(err);
          notify(
            platformProcess.video.id,
            "error",
            "ERROR ON Stage: FINAL",
            "General Error on Maestro"
          );
        });
    } else {
      console.log("ERROR: titles or video not rendered");
      console.log("###################################");
      console.log("Titles: ", platformProcess.titles);
      console.log("Video: ", platformProcess.video);
    }
  }

  cleanProcess(tmpPath) {
    rimraf(tmpPath, () => {
      delete this.processes[tmpPath];
    });
  }
}
