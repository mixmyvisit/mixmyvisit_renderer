import ExtractFrames from "../extract_frames";
//import fs from 'fs';
import _ from "lodash";
import axios from "axios";
const fs = require("fs");

export default new Promise(function (resolve, reject) {
  //if try to authenticate, more than
  //let maxRequests = 2;
  //let requests = 0;
  //console.log('test:' + process.cwd());

  console.log("WINDOW: " + location);

  let data = fs.readFileSync(
    "src\\headless_browser\\debug\\video.json",
    "utf8",
    function () {
      console.log(arguments);
    }
  );

  //let test = fs.readFileSync('video.json', 'utf8');
  //let data = JSON.parse(fs.readFileSync('video.json', 'utf8'));
  console.log("JSON: " + data);
  let video = JSON.parse(data).video;
  let metadata = JSON.parse(data).metadata;
  let id = JSON.parse(data).id;

  console.log("\n#####################################################");
  console.log("ID =>", id);
  console.log("METADATA =>", metadata);
  console.log("VIDEO =>", video);
  console.log("################################################\n");

  axios.get("http://127.0.0.1:6969/folder/temp").then(function (response) {
    let tmpPath = response.data.path;
    let formData = {
      path: tmpPath,
      id: id,
      video: video,
      metadata: metadata,
    };

    axios
      .post("http://127.0.0.1:6969/maestro/add", formData)
      .then(function (response) {
        // new ExtractFrames(
        //     tmpPath,
        //     video,
        //     metadata
        // ).renderTitles();

        let clipsFormData = { path: tmpPath };
        // console.log('EXTRACT_FRAMES');
        axios
          .post("http://127.0.0.1:6969/render/clips", clipsFormData)
          .then(function (response) {})
          .catch(function (error) {
            console.log("http://127.0.0.1:6969/render/clips error");
            console.log(error);
          });
      })
      .catch(function (error) {
        console.log("maestro add error");
        console.log(error);
      });
  });
});
