import ExtractFrames from "../extract_frames";
import io from "socket.io-client";
import Echo from "laravel-echo";
import _ from "lodash";
import axios from "axios";
require("dotenv").config();
import FormData from "form-data";
import notify, { headlessNotify } from "../../notify";

/*
This is the socket running in a headless-browser to await for new rendering requests for the backend

After it receives a request, it communicates to the local express server (local_requests\index.js) running outside of the headless-browser
(a different process than this one) actions that need to be done by Maestro (maestro\index.js), a wrapper to manage the render process.

After a rendering request is received, it does 3 things:

1. It asks Maestro to start a new process to render the video, the stages that don't run on the headless-browser (all except extract_frames)
2. It starts a process to render the title animations, in a headless-browser (extract_frames\index.js)
3. It asks Maestro to start the process to cut and concatenate the clips, adding transitions if present, as set up in number 1 (render\index.js)

After that, 2 and 3 will tell Maestro when their part is ready, and then Maestro will start the final render process, to render clips, stickers and audio
and assemble everything together
*/

export default new Promise(function (resolve, reject) {
  //if try to authenticate, more than
  //let maxRequests = 2;
  //let requests = 0;
  console.log("Connecting to socket service");

  connect(
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU2YzUyOTdlZmFkZjc2NGJkYzdkMDEzZGRlNDk2MTk5M2VkNGJjMDhmNDJhZmE2MjIzZDkwNmVjMGZhM2VhODliNDMwZDlhNjllMTA2MjdmIn0.eyJhdWQiOiIzIiwianRpIjoiZTZjNTI5N2VmYWRmNzY0YmRjN2QwMTNkZGU0OTYxOTkzZWQ0YmMwOGY0MmFmYTYyMjNkOTA2ZWMwZmEzZWE4OWI0MzBkOWE2OWUxMDYyN2YiLCJpYXQiOjE1Nzk2MTgyMjgsIm5iZiI6MTU3OTYxODIyOCwiZXhwIjoxNjExMjQwNjI4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.kClD87AsnQVlgatdn7HtpjT_jyQfqrtJ4SYGK6va540IYcrXum0Lu0Sn7bgPHdTfaJnRLDYFLHDcRfRAOvdtoXQObqBeJr9prz6JQN_dcm49lJMT3Rejx5aNSu1elSZdSVdx7SzdSDZhOfb6AyMwHLadogf_mweuegcg3EGk9-EpoBqnEYbQG5XrDNF5oyi6XZg9gCF-VUFB04qAZ-_OhNICJCXrHviPs3FB2R52l44CQcMQ0bnrqzbG9mqs_dyOwC2KdfzDQqF2vifH4L62KW607F3y7phTx4H82-H6kdizraYHO4nCyLKJN4LNt3muFwPROOU-WBrGUkJeBwtn6G1B711SLmPJaC5B6-W53jrkzT-NNr23BcPl_SHzuUrY-rRgJxbsPBNiYBbGQBwn07PQOD_aDfPiORCKJylqsxiOqqPMTUrctuw2VGQD7uVe9xHDnnBb5Ift18nNiPsrmxtB-fWdBq_l2PBvB8Crz8IaFHXyNXBAdpY54SroBGALmQdXPHh6Zk7CN7gnS8F_wpyZF-7FRuhv1M3fBhx_ztg4EfFSZaelJvteXLAKuFP5OMsVpC2DakatT5SVE_0DRIr_NpCqiBabU0oX1WfWf_iOyTG5g8mZZMy8sBgs9rOW5PuHFF8erFMz1-oPfWl82uAzX7UFrZ6nEd7yGQqs1KM"
  );

  function connect(credentials) {
    const echo = new Echo({
      broadcaster: "socket.io",
      host: `http://127.0.0.1:6001`,
      client: io,
      auth: {
        headers: {
          Authorization: `Bearer ${credentials}`,
        },
      },
    });

    echo.connector.socket.on("connect", function () {
      console.log("Connected");
      //requests = 0;
      resolve({ id: echo.socketId(), data: "success" });
    });

    echo.connector.socket.on("disconnect", function (err) {
      console.log("ERR", err);
      requestCredentials();
    });

    echo.connector.socket.on("reconnecting", function (attemptNumber) {
      console.log({
        error:
          "Server unavailable -> Attempting to reconnect -> Attempt number " +
          attemptNumber,
      });

      if (attemptNumber >= 10) {
        requestCredentials();
      }
    });

    echo.channel("VIDEO").listen("VideoRender", (data) => {
      const id = data.id;
      headlessNotify(
        id,
        "start",
        "STARTED RENDERING PROCESS: Parsing JSON",
        "0"
      );

      var message = JSON.parse(data.message);

      const username = message.username;
      const video = message.video;
      const metadata = message.metadata;
      const tagId = message.tag;
      headlessNotify(id, "progress", "FINISHED Parsing JSON", "100");
      console.log("\n#####################################################");

      try {
        console.log("ID =>", id);
        console.log("METADATA =>", metadata);
        console.log("USERNAME =>", username);
        console.log("VIDEO =>", video);
        console.log("TAGID =>", tagId);
      } catch (e) {
        headlessNotify(
          id,
          "error",
          "ERROR ON Stage: Parsing JSON",
          "empty field"
        );
        console.log('error?')
        throw new Error(e);
      }
      console.log("################################################\n");

      //It asks Maestro for a temp folder path to save temporary files
      axios
        .get("http://127.0.0.1:6969/folder/temp")
        .then(function (response) {
          let tmpPath = response.data.path;

          let formData = {
            path: tmpPath,
            id: id,
            video: video,
            tagId: tagId,
            username: username,
            metadata: metadata,
          };

          headlessNotify(
            id,
            "progress",
            "FINISHED Stage: Creating Temporary Folders"
          );

          axios.post("http://127.0.0.1:6969/maestro/add", formData)
              .then(function (response) {
                let clipsFormData = {path: tmpPath};

                axios
                    .post(
                        "http://127.0.0.1:6969/render/clips",
                        clipsFormData
                    )
                    .then(function (response) {
                    }).catch(function (error) {
                      //headlessNotify(id, "error", "ERROR ON Stage: Render Clips Request", "render/clips");
                      console.log('error sending to render/clips', error);
                    })
              }).catch(function (error) {
                headlessNotify(id, "error", "ERROR ON Stage: Maestro Add Request", "maestro/add");
              })
        })
        .catch(function (error) {
          headlessNotify(
            id,
            "error",
            "ERROR ON Stage: General Pre-Rendering Error"
          );
          console.log(
            "socket http://127.0.0.1:6969/folder/temp error =>",
            error
          );
        });
    });
  }
});
