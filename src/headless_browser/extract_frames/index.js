import axios from "axios";
import FormData from "form-data";
import lottie from "lottie-web";
import _ from "lodash";
import { createCanvas } from "canvas";
import titles from "../../templates/titles";
import {headlessNotify} from "../../notify";

//This is responsible for rendering the titles that are canvas animations from lottie-web plugin
//into a stream of images. As it uses Canvas internally, it's necessary to run this in a browser
//environment, in this case, a headless-browser (a browser without GUI).
//After ending the process, it communicates to the Maestro Process via a request the Titles are ok.

export default class {
	constructor(tmpPath, video, metadata) {
		this.video = video;
		this.metadata = metadata;
		this.tmpPath = tmpPath;
	}

	renderTitles() {
		const video = this.video;
		const metadata = this.metadata;
		const tmpPath = this.tmpPath;

		//loop each title and append the gif data to
		_.each(video.platforms, (platformObject, platform) => {
			platformObject.overlays.titles.forEach((title, key) => {
				axios
					.get(
						`${process.env.API_SERVER}/titles/${title.id}`,
						//`http://localhost:8000/api/titles/${title.id}`,
						{
							headers: {
								Authorization: `Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxNzIxY2FkZGEwZjI2ZGQzODQ3YTdkNGEzODY4YzE4ZTE0YjJhZmQxN2QyYWRlNDI4YTQ1YzdlYmIwOWQ3YmYyNWIzNjE4ZjU4NmM1YjUwIn0.eyJhdWQiOiI0IiwianRpIjoiNDE3MjFjYWRkYTBmMjZkZDM4NDdhN2Q0YTM4NjhjMThlMTRiMmFmZDE3ZDJhZGU0MjhhNDVjN2ViYjA5ZDdiZjI1YjM2MThmNTg2YzViNTAiLCJpYXQiOjE1NTQ3NDQ5MjMsIm5iZiI6MTU1NDc0NDkyMywiZXhwIjoxNTg2MzY3MzIzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Hn519TBCWIfNelLtC_-17_zyJTL9e75qg9PLS67tOVsgf7TbJ6opQEnTpPKCv-O4pCue-rU7qS5AJ3nmPRMr-OdkbdwM_mWJJduc-saUU8c36hy7p1gxagutKHcLrGGBbtW0g9fOOAqqpK7ojQl0wnsuQ8vowsfqh6zCLB9IC6sq-NbZIv6EDRj7j90bFmlP3sni6QKfXos8FP17elZBbX3UBmV4P31IZ6nb5c-7ET1z5h1AQh1kuRQl0kdKp7s049sb8IAkGxOh2Y9kQyfgIlEHq9YtI7EZc3LXQKUrqNiUV3xojsx9YSkdYCvHxhg8dIu7ecr-CgLJ6Ye1t9x7RwWTV9zMxDCo6jcEV2zJPXmXAmUtaRRPhr3upGK0OLQkoLRJXxasg2bQRdUuNnnNmSb8DHqT-tPdpVbJo-6Up-UBgqQuUnpLSm1wH60qaQ7coukjeExrNxMgG-tSqGLTGzDjyUmeNjbBbNgLxFGk_7CMfbD_1TtrWKfpoNU3KUt7Eh7nyUpDi3J02b1Vc2yqvXbWgubxPpt51N_vC2yQVs-dPv2ykcozHxmmaopkhunjGPnvTistHUJk2r5Mh21ncmW4bAd-k4lckrj9x18LE9zj6dY-DK7bbG-kTySAPtehoZ44ZrMUYNSwRFvo6lXpEjj9YZZ97c04yl0c_BHFclo`
							}
						}
					)
					.then(function(response) {
						let responseData = response.data;
						axios
							.get(responseData.data.url)
							.then((response) => {
								let responseData = response.data;
								let formData = new FormData();
								//create title filename for ffmpeg to render and save
								formData.append("path", tmpPath);
								formData.append("filename", `title`);
								formData.append("platform", platform);
								formData.append("index", key);
								let frames = 0;
								let images = [];

								const canvas = createCanvas(
									metadata.resolution.width,
									metadata.resolution.height
								);

								let animationData = responseData;

									animationData = JSON.parse(
										JSON.stringify(animationData).replace(
											/"TEXT1"/gi,
											`"${title.title}"`
										)
									);

								animationData = JSON.parse(
									JSON.stringify(animationData).replace(
										/"TEXT2"/gi,
										`"${title.subtitle}"`
									)
								);

								let canvasAnim = lottie.loadAnimation({
									renderer: "canvas",
									loop: false,
									autoplay: false,
									animationData: animationData,
									rendererSettings: {
										context: canvas.getContext("2d"), // the canvas context
										scaleMode: "noScale",
										clearCanvas: true
									},
									assetsPath: "http://dev-cofina-vm02.westeurope.cloudapp.azure.com/templates/template_00/titles/images/"
								});

								canvasAnim.frameRate = 24;

								function getFrame() {
									canvasAnim.goToAndStop(frames, true);

									images.push(canvas.toDataURL());

									frames++;
									if (frames <= canvasAnim.getDuration(true)) {
										canvas.toBlob(blob => {
											formData.append("frames", blob);
											getFrame();
										});
									} else {
										axios
											.post(
												"http://127.0.0.1:6969/render/title",
												formData
											)
											.then(function(response) {
												console.log(response.status);
											})
											.catch(function(error) {
												console.log(error);
											});
									}
								}
								getFrame();
							})
							.catch(function(error) {
								console.log(error);});
					})
					.catch(function(error) {
						console.log(error);
					});
			});
		});
	}
}
