import {} from "dotenv/config";
import browserify from "browserify";
import babelify from "babelify";
import browser from "browser-run";
import localRequests from "./local_requests";
import mkdirp from "mkdirp";
import exorcist from "exorcist";

/*
IN CASE THE RENDERING IS GOINT TO BE CPU BASED INSTEAD OF GPU
CERTAIN DEPENDENCIES NEED TO BE INSTALLED

Create an instance of an headlessBrowser to run the webgl videoContext
in order to get the buffer data from the video frames and pipe it
to a process where video and audio are merged using ffmpeg.

.pipe(browser) opens a socket connection to the electron browser
which is run with the package browser-run

The pipe to render is defined in the package.json file 

$ sudo npm install electron --unsafe-perm=true
$ sudo apt-get install -y libgtk2.0-0 libnotify-bin libgconf-2-4 libnss3 xvfb libxss1 ffmpeg
$ sudo add-apt-repository ppa:jonathonf/ffmpeg-4
// for Ubuntu 14.04 only
	$ sudo add-apt-repository ppa:jonathonf/tesseract
//
$ sudo apt update
$ sudo apt-get install ffmpeg
$ export DISPLAY=':99.0'	
$ Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
*/

/*
To debug application with a locally provided JSON, change browserfy module below
from ./src/headless_browser/socket to ./src/headless_browser/debug
Also make sure a {port: <port>} is not specified inside browser() call (browser-run module),
otherwise browser-run will wait for a browser to be opened at localhost:port to start
*/

/*
APPLICATION ARCHITECTURE

REGULAR MAIN PROCESS
-> LOCAL REQUESTS EXPRESS SERVER (+MAESTRO)
-> 2º STAGE RENDERING PROCESS: CUT & CONCAT CLIPS, ADD TRANSITIONS (FORK FROM REGULAR MAIN)
-> FINAL STAGE RENDERING PROCESS: MIX AND MUX EVERYTHING, CLIPS, AUDIO AND STICKERS (BELONGS TO ABOVE)

HEADLESS BROWSER PROCESS
-> SOCKET SERVER TO LISTEN FOR NEW RENDERING REQUESTS
-> 1ª STAGE RENDERING PROCESS: RENDER TITLES USING CANVAS FROM LOTTIE-WEB PLUGIN (FORK FROM HEADLESS)
 */

//create directories that don`t exist with 0750 permission
const mapfile = __dirname + 'bundle.js.map';
console.log("Bundle " + mapfile);

var oldmask = process.umask(0);
mkdirp(process.env.CDN_PATH, "0750", function(err) {
	console.log(process.env.CDN_PATH);
	process.umask(oldmask);
	if (err) {
		console.error(err);
	} else {
		localRequests                                 //Creates express.js local server for IPC between headless and regular process
			.then(() => {                   //This is Maestro
				console.log("Local Requests Server for IPC created");
				browserify("./src/headless_browser/socket", {debug: true})
					.transform("babelify", {           //Makes the necessary transformations for browser compatibility
						presets: ["@babel/preset-env"] //using present-env (basic babel preset)
					})
					.transform('brfs')                 //Special module to make readAsync work inside headless browser by 'inlining" fs file content
					.bundle()                          //Bundles everything inside an .js file and includes in <script>, reading through requires
					.pipe(exorcist(mapfile))           //Exports sorcemaps to specified mapfile, so debuging with IDE works properly
					.pipe(browser())                   //Redirects output of browserify to run inside a headless browser environment
					.pipe(process.stdout);             //Redirects stdout from browser-run to process.stdout
			})
			.catch(err => {
				console.log("Error creating local server", err);
			});
	}
});