export default () => {
	return {
		CROSSFADE: { duration: 1000, name: "fade" },
		DREAMFADE: { duration: 1000, name: "dreamy" },
		HORIZONTAL_WIPE: {
			duration: 1000,
			name: "fade",
			params: { direction: [1, -1] }
		},
		VERTICAL_WIPE: {
			duration: 1000,
			name: "fade",
			params: { direction: [-1, 1] }
		}
	};
};
