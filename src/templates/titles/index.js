import * as Facebook from "./animations/facebook";
import * as Twitter from "./animations/twitter";

export default {
	FACEBOOK: {
		slide_left: Facebook.slide_left
	},
	TWITTER: {
		slide_left: Twitter.slide_left
	}
};
