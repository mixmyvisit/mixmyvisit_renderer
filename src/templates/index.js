import titles from "./titles";
import transitions from "./transitions";

export default {
	TEMPLATE_01: {
		FACEBOOK: {
			titles: [titles.FACEBOOK.slide_left],
			transitions: {},
			effects: {}
		},
		TWITTER: {
			titles: [titles.TWITTER.slide_left],
			transitions: {},
			effects: {}
		}
	}
};
